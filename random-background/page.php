<?php get_header(); ?>

<!-- start page -->
<div class="main">		
		
		<div class="content">

<!-- Start of  Blog Entry -->
							<?php
								if (have_posts())
								{
										 while (have_posts()) 
										 {
										 the_post();
							?>		
									<div id="page-<?php echo the_ID();?>" class="<?php echo $alternate;?>">
													<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
													<!--
													<h4><a href="<?php the_author_url(); ?>"><?php the_author() ?></a> at <?php the_date() ?> : <?php the_time() ?>  <?php edit_post_link(__('Edit This')); ?></h4>
													-->
													<?php if ( get_post_meta($post->ID, 'heading', true) ) 
													{ 
													?> 
														<h4><?php echo get_post_meta($post->ID, "heading", $single = true); ?></h4>
													<?php 
													}
													?>													
													
													<div class="post-body">
													<?php the_content('Read the rest of this entry &raquo;'); ?>
													</div>
										</div>	 
							<?php
										 }
							?>
							
							<?php			 
								}else{
								// No Post Found
								?>
								
								<div id="errobox" style="padding-left:10px">
									<h2 class="center">404 Not Found</h2>
									<p class="center">Sorry, but you are looking for something that isn't here.</p>
								</div>	
								
								<?php
								}
							?>
	<!-- end of content -->
	</div>
	<?php get_sidebar(); ?>		
	<div class="clear"></div>
 </div>
<!-- end of start page -->