<div class="sidenav2"><div class="sidenav3"><div class="sidenav">
<ul>
	<?php
	if( function_exists('dynamic_sidebar') && dynamic_sidebar('columnLeft') )
	{

	}else{ 
	?>
	
	<li class="sidebar-cat">
		<h2><?php _e('Categories'); ?></h2>
		<ul>
		<?php wp_list_categories('title_li=&hierarchical=0'); ?>
		</ul>
	</li>
	
	<li class="sidebar-archieves">
		<h2>Archives</h2>
		<ul>
		<?php wp_get_archives('type=monthly'); ?>
		</ul>
	</li>
	
	<li class="sidebar-meta">
	<h2>Meta</h2>
	<ul>
		<?php wp_register(); ?>
		<li><?php wp_loginout(); ?></li>
		<li><a href="http://validator.w3.org/check/referer" title="This page validates as XHTML 1.0 Transitional">Valid <abbr title="eXtensible HyperText Markup Language">XHTML</abbr></a></li>
		<li><a href="http://gmpg.org/xfn/"><abbr title="XHTML Friends Network">XFN</abbr></a></li>
		<li><a href="http://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress</a></li>
		<?php wp_meta(); ?>
	</ul>
	</li>		
		
	<?php
	}
	?>
</ul>	
</div></div></div><!-- sidebar -->